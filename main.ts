const FRAMES = 5;
const THROWES = 3;
const PINS = 15;
const STRIKE = "*";
const SPARE = "/";

class Counting {
  #scores = Array.from({ length: FRAMES }, () => 0);
  #party = Array.from({ length: FRAMES }, () =>
    Array.from({ length: THROWES }, () => "")
  );
  #pins = PINS;
  #hasStrike = false;
  #hasSpare = false;
  #currentStrikeIdx?: number;
  #currentSpareIdx?: number;
  #isLastFrame = false;
  #data: string[][];
  score = 0;
  constructor(data: string[][]) {
    console.log(
      `(🎳) Test Game: ${FRAMES} Frame(s), ${THROWES} Throw(es), ${PINS} Pins`
    );
    this.#data = data;
    this.process();
  }

  process() {
    for (let i = 0; i < FRAMES; i++) {
      this.#isLastFrame = i === FRAMES - 1;
      for (let j = 0; j < THROWES; j++) {
        const value = this.#data.length
          ? this.#data[i][j]
          : (
              prompt(`Frames/Throw: [${i + 1}/${j + 1}]: `) ?? ""
            ).toLocaleLowerCase();
        if (!value) throw new Error("Invalid input!");
        if (isNaN(+value) && ![STRIKE, SPARE].includes(value)) {
          throw new Error("Invalid input!");
        }
        if (value === STRIKE && j > 0) {
          throw new Error("Strike is only allowed on the first throw!");
        }
        if (value === SPARE && j === 0) {
          throw new Error("Spare is only allowed on the second throw!");
        }
        if (+value < 0 || +value > PINS) throw new Error("Value out of range!");
        this.#party[i][j] = value!;
        if (value === STRIKE) {
          this.#pins = PINS;
          this.#hasStrike = true;
          this.#currentStrikeIdx = i + 1;
          if (this.#isLastFrame) {
            for (let k = 1; k <= THROWES; k++) {
              if (this.#data.length) {
                this.#party[i][k] = this.#data[i][k];
                this.#calculateLastFrame(i);
                continue;
              }
              this.#party[i][k] =
                prompt(`Frames/Throw: [${i + 1}/${j + 2}]: `) ?? "";
            }
            if (!this.#data.length) {
              this.#calculateLastFrame(i);
            }
          }
          break;
        }
        if (value === SPARE) {
          this.#hasSpare = true;
          this.#currentSpareIdx = i + 1;
          this.#pins = PINS;
          if (this.#isLastFrame) {
            let k = j === 2 ? 1 : 2;
            let l = j === 2 ? THROWES + 1 : j + 2;
            if (this.#data.length) {
              for (; k <= l; k++) {
                this.#party[i][k] = this.#data[i][k];
              }
              this.#calculateLastFrame(i);
            } else {
              for (let k = 1; k <= THROWES; k++) {
                this.#party[i][k] =
                  prompt(`Frames/Throw: [${i + 1}/${j + 2}]: `) ?? "";
              }
              this.#calculateLastFrame(i);
            }
          }
          break;
        }
        this.#pins -= +value;
      }
      if (!this.#isLastFrame) {
        this.#countFrameScore(i);
      } else {
        this.#calculateLastFrame(i);
      }
      // Reset pins each frame
      this.#pins = PINS;
    }
    console.log(this.#scores);
    this.score = this.#scores.reduce((acc, curr) => acc + curr, 0);
    console.log(`The total score is: ${this.score}`);
  }

  #calculateLastFrame(i: number) {
    const throws = this.#party[i]; // 3 throws [x, value, value, value]

    for (let thr = 0; thr < throws.length; thr++) {
      if (throws[thr] === STRIKE) {
        this.#scores[i] = PINS;
      } else if (throws[thr] === SPARE) {
        this.#scores[i] = PINS;
      } else {
        this.#scores[i] += Number(throws[thr]);
      }
    }
    if (i > 0) {
      this.#scores[i] += this.#scores[i - 1];
    }
  }

  #countFrameScore(frame: number) {
    const throws = this.#party[frame];
    if (
      !this.#isLastFrame &&
      this.#hasStrike &&
      this.#currentStrikeIdx === frame
    ) {
      this.#scores[this.#currentStrikeIdx - 1] += throws.reduce(
        (acc, curr) => acc + +curr,
        0
      );
      this.#hasStrike = false;
      this.#currentStrikeIdx = undefined;
    }

    if (
      !this.#isLastFrame &&
      this.#hasSpare &&
      this.#currentSpareIdx === frame
    ) {
      this.#scores[this.#currentSpareIdx - 1] +=
        Number(throws[0]) + Number(throws[1]);
      this.#hasSpare = false;
      this.#currentSpareIdx = undefined;
    }

    if (throws[0] === STRIKE && !this.#isLastFrame) {
      this.#scores[frame] = PINS;
    } else if (
      (throws[1] === SPARE || throws[2] === SPARE) &&
      !this.#isLastFrame
    ) {
      this.#scores[frame] += PINS;
    } else {
      if (!this.#isLastFrame) {
        this.#scores[frame] = throws.reduce((acc, curr) => acc + +curr, 0);
      } else if (this.#isLastFrame && throws.length >= 3) {
        this.#scores[frame] +=
          this.#scores[frame - 1] +
          throws.reduce((acc, curr) => acc + +curr, 0);
      }
    }
    if (frame > 0 && !this.#isLastFrame) {
      this.#scores[frame] += this.#scores[frame - 1];
    }
  }
}
// MOCK DATA

// const t = [
//   ["2", SPARE],
//   ["6", "2", "2"],
//   [STRIKE],
//   ["1", "2", "7"],
//   [STRIKE, "1", "1", "1"],
// ];
// const t = [
//   ["1", "1", "1"],
//   ["1", "1", "1"],
//   ["1", "1", "1"],
//   ["1", "1", "1"],
//   ["1", "2", "3"],
// ];
